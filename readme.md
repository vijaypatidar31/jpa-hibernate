# JPA With Hibernate

## JPA - Java Persistence API

The Java Persistence API (JPA) is the Java standard for mapping Java objects to a relational database.

JPA is one possible approach to ORM. Via JPA, the developer can map, store, update, and retrieve data from relational
databases to Java objects and vice versa.

JPA is a specification and several implementations are available.

### JPA implementations:

* Hibernate: The most advanced and widely used. Pay attention for the classpath because a lot of libraries are used,
  especially when using JBoss. Supports JPA 2.1.
* Toplink: Only supports the basic JPA specs. (This was oracle’s free version of the JPA implementation)
* EclipseLink: Is based on TopLink, and is the intended path forward for persistence for Oracle and TopLink. Supports
  JPA 2.1
* Apache OpenJPA: Best documentation but seems very buggy. Open source implementation for JPA. Supports JPA 2.0
* DataNucleus: Well documented, open source (Apache 2 license), is also a JDO provider. Supports JPA 2.1
* ObjectDB: well documented
* CMobileCom JPA: light-weight JPA 2.1 implementation for both Java and Android.

## Hibernate

Hibernate ORM is an object–relational mapping tool for the Java programming language. It provides a framework for
mapping an object-oriented domain model to a relational database. Hibernate also provide JPA implementation which most
popular than other JPA implementations.

# JPA vs Hibernate

| JPA | Hibernate |
|-------|------|
|Java Persistence API (JPA) defines the management of relational data in the Java applications.|Hibernate is an Object-Relational Mapping (ORM) tool which is used to save the state of Java object into the database.|
|It is just a specification. Various ORM tools implement it for data persistence.    |It is one of the most frequently used JPA implementation.
|It is defined in javax.persistence package.    |It is defined in org.hibernate package.
|The EntityManagerFactory interface is used to interact with the entity manager factory for the persistence unit. Thus, it provides an entity manager.    |It uses SessionFactory interface to create Session instances.
|It uses EntityManager interface to create, read, and delete operations for instances of mapped entity classes. This interface interacts with the persistence context. |It uses Session interface to create, read, and delete operations for instances of mapped entity classes. It behaves as a runtime interface between a Java application and Hibernate.
|It uses Java Persistence Query Language (JPQL) as an object-oriented query language to perform database operations.| It uses Hibernate Query Language (HQL) as an object-oriented query language to perform database operations.

#### Reference [JAVATPOINT](https://www.javatpoint.com/jpa-vs-hibernate) 