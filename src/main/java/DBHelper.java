import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DBHelper {
    public static void main(String[] args) {
        DBHelper helper = new DBHelper();

        User user = new User(
                "Vijay",
                "vijay@example.com"
        );

        helper.saveObject(user);

        System.out.println(helper.getObject(user.getId(), User.class));
    }

    public EntityManager entityManager() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");
        return emf.createEntityManager();
    }

    public <T> T getObject(Object id, Class<T> a) {
        EntityManager entityManager = entityManager();
        return entityManager.find(a, id);
    }


    public void saveObject(Object object) {
        EntityManager entityManager = entityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(object);
        transaction.commit();
    }
}
